import React, { Fragment } from 'react'
import CardProduct from '../CardProduct/CardProduct'
import './Product.css'

class Product extends React.Component {
  state = {
    order: 4
  }

  handleCounterChange = (newValue) => {
    this.setState({
      order: newValue
    })
  }

  render() {
    return (
      <Fragment>
        <div className="parent">
          <h5 className="count">{this.state.order}</h5>
        </div>
        <CardProduct onCounterChange={(value) => this.handleCounterChange(value)} />
      </Fragment>
    )
  }
}

export default Product