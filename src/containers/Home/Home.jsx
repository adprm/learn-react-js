import React from 'react'
import YoutubeComp from '../../components/YoutubeComp/YoutubeComp'
import Product from '../Product/Product'

class Home extends React.Component {
  render() {
    return (
      <div>
        <p>Youtube Component</p>
        <hr />
        <YoutubeComp time='18:10' title='Live musik konser 1' desc='216 rb x ditonton' />
        <YoutubeComp time='10:00' title='Live musik konser 2' desc='100 rb x ditonton' />
        <YoutubeComp time='05:21' title='Live musik konser 3' desc='1 rb x ditonton' />
        <YoutubeComp time='10:01' title='Live musik konser 4' desc='124 rb x ditonton' />
        <YoutubeComp />

        <p>Counter</p>
        <hr />
        <Product />
      </div>
    )
  }
}

export default Home