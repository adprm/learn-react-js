import React from 'react'
import './YoutubeComp.css'

const YoutubeComp = (props) => {
  return (
    <div className="youtube-wrapper">
      <div className="img-thumb">
        <img src="https://i.ytimg.com/vi/swBbQKft5jM/hqdefault.jpg?sqp=-oaymwEcCNACELwBSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLCvSDJLrtchtgPsifpjsJGGeXvclw" alt="" />
        <p className="p-time">{props.time}</p>
      </div>
      <p className="p-title">{props.title}</p>
      <p className="p-desc">{props.desc}</p>
    </div>
  )
}

YoutubeComp.defaultProps = {
  time: '00:00',
  title: 'Title Here',
  desc: 'xx di tonton',
}

export default YoutubeComp